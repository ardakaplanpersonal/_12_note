package com.androidegitim.not;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.androidegitim.androidegitimlibrary.helpers.SharedPreferencesHelpers;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

    private static final String TEXT_KEY = "TEXT_KEY";

    @BindView(R.id.main_edittext_note)
    EditText noteEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        //shared preferences test verisi kaydetme
        SharedPreferencesHelpers.putBoolean(this, "BOOLEAN", true);
        SharedPreferencesHelpers.putInt(this, "INT", 1569);

        noteEditText.setText(SharedPreferencesHelpers.getString(this, TEXT_KEY));
    }

    @Override
    protected void onPause() {
        super.onPause();

        String note = noteEditText.getText().toString().trim();

        SharedPreferencesHelpers.putString(getApplicationContext(), TEXT_KEY, note);

        //shared preferences test verisi görme
        Log.i("TAG", "boolean : " + SharedPreferencesHelpers.getBoolean(this, "BOOLEAN"));
        Log.i("TAG", "int : " + SharedPreferencesHelpers.getInt(this, "INT"));
    }
}
